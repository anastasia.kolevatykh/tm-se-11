package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class DeserializationLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "l";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad data from file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[LOAD (deserialization)]");
        serviceLocator.getDomainEndpoint().deserialize(session);
        System.out.println("[Loading is successful!]");
    }
}
