package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Exception_Exception;
import ru.kolevatykh.tm.endpoint.Session;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskEndpoint().removeAllTasks(session);
        System.out.println("[Removed all tasks.]");
        System.out.println("[OK]");
    }
}
