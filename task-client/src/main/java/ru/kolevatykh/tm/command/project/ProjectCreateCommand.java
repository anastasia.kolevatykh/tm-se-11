package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter project description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project start date: ");
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project end date: ");
        @NotNull final String endDate = ConsoleInputUtil.getConsoleInput();

        serviceLocator.getProjectEndpoint().persistProject(session, name, description, startDate, endDate);
        System.out.println("[OK]");
    }
}
