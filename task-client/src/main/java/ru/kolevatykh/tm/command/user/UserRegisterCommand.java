package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;
import ru.kolevatykh.tm.endpoint.User;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class UserRegisterCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ur";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRegister new user.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session != null) {
            throw new Exception("[You are authorized under login: "
                    + serviceLocator.getUserEndpoint().findUserById(session).getLogin()
                    + "\nPlease LOGOUT first, in order to authorize under OTHER account.]");
        }

        System.out.println("[USER REGISTRATION]\nEnter your login: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final User user = serviceLocator.getUserEndpoint().findUserByLogin(login);
        if (user != null) {
            throw new Exception("[Login '" + login + "' already exists. Please, retry.]");
        }

        System.out.println("Enter your password: ");
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();
        if (password.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        @Nullable final Session sessionNew = serviceLocator.getSessionEndpoint().openSession(login, password);
        serviceLocator.setSession(sessionNew);
        System.out.println("[OK]");
    }
}
