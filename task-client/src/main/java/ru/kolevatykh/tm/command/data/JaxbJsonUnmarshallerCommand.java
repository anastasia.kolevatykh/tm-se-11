package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class JaxbJsonUnmarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "json-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ju";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad from json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[JSON UNMARSHALLER]");
        serviceLocator.getDomainEndpoint().unmarshalJaxbJson(session);
        System.out.println("[Loading is successful!]");
    }
}
