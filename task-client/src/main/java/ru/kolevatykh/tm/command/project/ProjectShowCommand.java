package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.wrapper.TaskWrapper;
import java.lang.Exception;
import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "psh";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[PROJECT SHOW]\nEnter project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByName(session, name);
        if (project == null) {
            throw new Exception("[The project '" + name + "' does not exist!]");
        }

        @NotNull final String id = project.getId();

        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint().findTasksByProjectId(session, id);
        if (taskList != null) {
            System.out.println("[TASK LIST]");

            @NotNull final StringBuilder projectTasks = new StringBuilder();
            int i = 0;

            for (@NotNull final Task task : taskList) {
                if (id.equals(task.getProjectId())) {
                    TaskWrapper taskWrapper = new TaskWrapper(task);
                    projectTasks
                            .append(++i)
                            .append(". ")
                            .append(taskWrapper.toString())
                            .append(System.lineSeparator());
                }
            }

            @NotNull final String taskString = projectTasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
