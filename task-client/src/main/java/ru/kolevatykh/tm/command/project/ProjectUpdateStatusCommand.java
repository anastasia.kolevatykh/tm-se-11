package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;

public final class ProjectUpdateStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[PROJECT UPDATE STATUS]\nEnter project id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(session, id);
        if (project == null) {
            throw new Exception("[The project id '" + id + "' does not exist!]");
        }

        System.out.println("Enter new STATUS name: PLANNED INPROCESS READY");
        @NotNull final String status = ConsoleInputUtil.getConsoleInput();
        if (status.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        serviceLocator.getProjectEndpoint().mergeProjectStatus(session, id, status);
        System.out.println("[OK]");
    }
}
