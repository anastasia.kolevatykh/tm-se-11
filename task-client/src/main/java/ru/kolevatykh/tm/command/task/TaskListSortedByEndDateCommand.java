package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.RoleType;
import ru.kolevatykh.tm.endpoint.Session;
import ru.kolevatykh.tm.endpoint.Task;
import ru.kolevatykh.tm.wrapper.TaskWrapper;

import java.util.ArrayList;
import java.util.List;

public final class TaskListSortedByEndDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list-sorted-ed";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tlsed";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all tasks sorted by end date.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[TASK LIST]");
        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint().findTasksSortedByEndDate(session);
        if (taskList == null) {
            throw new Exception("[No tasks yet.]");
        }

        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;

        for (@NotNull final Task task : taskList) {
            TaskWrapper taskWrapper = new TaskWrapper(task);
            tasks
                    .append(++i)
                    .append(". ")
                    .append(taskWrapper.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
