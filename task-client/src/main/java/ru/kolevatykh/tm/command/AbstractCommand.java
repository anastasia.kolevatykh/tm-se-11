package ru.kolevatykh.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.ServiceLocator;
import ru.kolevatykh.tm.endpoint.RoleType;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractCommand(@NotNull final ServiceLocator serviceLocator) {
        this.setServiceLocator(serviceLocator);
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getShortName();

    @NotNull
    public abstract String getDescription();

    public abstract boolean needAuth();

    public abstract void execute() throws Exception;
}
