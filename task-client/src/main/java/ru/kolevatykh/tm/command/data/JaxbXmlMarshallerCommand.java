package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class JaxbXmlMarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "xml-marshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave all projects and tasks to xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[XML MARSHALLER]");
        serviceLocator.getDomainEndpoint().marshalJaxbXml(session);
        System.out.println("[Saving is successful!]");
    }
}
