package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.endpoint.Session;
import ru.kolevatykh.tm.wrapper.ProjectWrapper;
import java.util.List;

public final class ProjectListSortedByEndDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list-sorted-ed";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "plsed";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects sorted by end date.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[PROJECT LIST]");
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findProjectsSortedByEndDate(session);
        if (projectList == null) {
            throw new Exception("[No projects yet.]");
        }

        @NotNull final StringBuilder projects = new StringBuilder();
        int i = 0;

        for (@NotNull final Project project : projectList) {
            ProjectWrapper projectWrapper = new ProjectWrapper(project);
            projects
                    .append(++i)
                    .append(". ")
                    .append(projectWrapper.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String projectString = projects.toString();
        System.out.println(projectString);
    }
}
