package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class JacksonLoadXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-Load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[JACKSON XML LOAD]");
        serviceLocator.getDomainEndpoint().loadJacksonXml(session);
        System.out.println("[Saving is successful!]");
    }
}
