package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;
import ru.kolevatykh.tm.endpoint.Task;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[TASK REMOVE]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(session, id);
        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        serviceLocator.getTaskEndpoint().removeTask(session, id);
        System.out.println("[OK]");
    }
}
