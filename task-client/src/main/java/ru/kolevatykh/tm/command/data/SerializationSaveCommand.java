package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class SerializationSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "s";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave data to file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[SAVE (serialization)]");
        serviceLocator.getDomainEndpoint().serialize(session);
        System.out.println("[Saving is successful!]");
    }
}
