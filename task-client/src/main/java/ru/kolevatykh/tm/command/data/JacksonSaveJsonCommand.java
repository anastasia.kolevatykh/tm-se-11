package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Session;

public final class JacksonSaveJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-json-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "js";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to json file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[JACKSON JSON SAVE]");
        serviceLocator.getDomainEndpoint().saveJacksonJson(session);
        System.out.println("[Saving is successful!]");
    }
}
