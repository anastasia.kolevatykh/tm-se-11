package ru.kolevatykh.tm.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "e";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\t\tExit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() {
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
