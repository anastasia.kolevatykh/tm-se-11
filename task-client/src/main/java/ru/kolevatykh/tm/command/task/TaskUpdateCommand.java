package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) return;
        System.out.println("[TASK UPDATE]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(session, id);
        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        System.out.println("Enter project id: ");
        @NotNull final String projectId = ConsoleInputUtil.getConsoleInput();
        if (projectId.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(session, projectId);
        if (project == null) {
            throw new Exception("[The task '" + projectId + "' does not exist!]");
        }

        System.out.println("Enter new task name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new task description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task start date: ");
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task end date: ");
        @NotNull final String endDate = ConsoleInputUtil.getConsoleInput();

        serviceLocator.getTaskEndpoint().mergeTask(session, projectId, id, name, description, startDate, endDate);
        System.out.println("[OK]");
    }
}
