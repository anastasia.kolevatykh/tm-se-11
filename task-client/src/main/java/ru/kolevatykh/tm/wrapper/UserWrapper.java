package ru.kolevatykh.tm.wrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.endpoint.User;

@Setter
@Getter
@NoArgsConstructor
public class UserWrapper extends User {

    @NotNull
    private User user;

    public UserWrapper(@NotNull final User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "id: '" + user.getId() + '\'' +
                ", login: '" + user.getLogin() + '\'' +
                ", roleType: " + user.getRoleType() +
                ", auth: " + user.isAuth();
    }
}
