package ru.kolevatykh.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;

import java.util.Collection;

public interface ServiceLocator {

    @Nullable Session getSession();

    void setSession(@Nullable Session session);

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull IDomainEndpoint getDomainEndpoint();

    @NotNull ISessionEndpoint getSessionEndpoint();

    @NotNull Collection<AbstractCommand> getCommands();
}
