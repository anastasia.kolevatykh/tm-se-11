package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull IUserService getUserService();

    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IDomainService getDomainService();

    @NotNull ISessionService getSessionService();
}
