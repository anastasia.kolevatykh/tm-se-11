package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {
    @Nullable List<T> findAll();

    @Nullable T findOneById(@Nullable String id);

//    void persist(@Nullable T entity);
//
//    void persist(@Nullable String login, @Nullable String password,
//                 @Nullable RoleType roleType, @Nullable Boolean isAuth);

//    void merge(@Nullable T entity);

    void remove(@Nullable String id);

    void removeAll();
}
