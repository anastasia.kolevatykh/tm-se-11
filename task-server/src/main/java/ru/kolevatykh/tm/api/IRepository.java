package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.List;

public interface IRepository<T> {
    @NotNull List<T> findAll();

    @Nullable T findOneById(@NotNull String id);

//    void persist(@NotNull T entity);
//
//    void persist(@NotNull String login, @NotNull String password,
//                 @NotNull RoleType roleType, @NotNull Boolean isAuth);

//    void merge(@NotNull T entity);

    void remove(@NotNull String id);

    void removeAll();
}
