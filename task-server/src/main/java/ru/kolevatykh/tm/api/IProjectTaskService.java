package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService<T> {
    @Nullable List<T> findAll();

    @Nullable List<T> findAllByUserId(@Nullable String userId);

    @Nullable T findOneById(@Nullable String userId, @Nullable String id);

    @Nullable T findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable final T entity);

    void mergeStatus(@Nullable String userId, @Nullable String id, @Nullable String name);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    @Nullable List<T> findAllSortedByStartDate(@Nullable final String userId);

    @Nullable List<T> findAllSortedByEndDate(@Nullable final String userId);

    @Nullable List<T> findAllSortedByStatus(@Nullable final String userId);

    @Nullable List<T> findAllBySearch(@Nullable final String userId, @Nullable final String search);
}
