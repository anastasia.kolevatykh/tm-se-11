package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.api.IProjectRepository;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService {

    @Nullable
    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.setProjectRepository(projectRepository);
    }

    @Override
    public void persist(@Nullable final String userId,
                        @Nullable final String name,
                        @Nullable final String description,
                        @Nullable final Date startDate,
                        @Nullable final Date endDate) {
        if (projectRepository == null
                || userId == null || userId.isEmpty()
                || name == null || name.isEmpty()) return;
        projectRepository.persist(userId, name, description, startDate, endDate);
    }

    @Override
    public void merge(@Nullable final String userId,
                      @Nullable final String id,
                      @Nullable final String name,
                      @Nullable final String description,
                      @Nullable final Date startDate,
                      @Nullable final Date endDate) {
        if (projectRepository == null
                || userId == null || userId.isEmpty()
                || id == null || id.isEmpty()
                || name == null || name.isEmpty()) return;
        projectRepository.merge(userId, id, name, description, startDate, endDate);
    }
}
