package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.*;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint() {
    }

    public DomainEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void serialize(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().serialize();
    }

    @Override
    @WebMethod
    public void deserialize(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().deserialize();
    }

    @Override
    @WebMethod
    public void saveJacksonJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().saveJacksonJson();
    }

    @Override
    @WebMethod
    public void loadJacksonJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().loadJacksonJson();
    }

    @Override
    @WebMethod
    public void saveJacksonXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().saveJacksonXml();
    }

    @Override
    @WebMethod
    public void loadJacksonXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().loadJacksonXml();
    }

    @Override
    @WebMethod
    public void marshalJaxbJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().marshalJaxbJson();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().unmarshalJaxbJson();
    }

    @Override
    @WebMethod
    public void marshalJaxbXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().marshalJaxbXml();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDomainService().unmarshalJaxbXml();
    }
}
