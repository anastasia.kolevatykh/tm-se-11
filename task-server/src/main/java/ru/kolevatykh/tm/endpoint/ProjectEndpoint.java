package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findAllProjects() {
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findAllProjectsByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final Project findProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public final Project findProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().persist(session.getUserId(), name, description,
                DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().merge(session.getUserId(), id, name, description,
                DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeProjectStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().mergeStatus(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByStartDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllSortedByStartDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByEndDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllSortedByEndDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByStatus(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllSortedByStatus(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsBySearch(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllBySearch(session.getUserId(), search);
    }
}
