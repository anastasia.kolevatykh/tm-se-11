package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public final List<Task> findAllTasks() {
        return serviceLocator.getTaskService().findAll();
    }

    @Nullable
    @WebMethod
    public final List<Task> findAllTasksByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @Nullable
    @WebMethod
    public final Task findTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public final Task findTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().persist(session.getUserId(), projectId, name, description,
               DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().merge(session.getUserId(), projectId, id, name, description,
                DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeTaskStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().mergeStatus(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByStartDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllSortedByStartDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByEndDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllSortedByEndDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByStatus(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllSortedByStatus(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksBySearch(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllBySearch(session.getUserId(), search);
    }

    @Override
    @WebMethod
    public void removeTasksWithProjectId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeTasksWithProjectId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeProjectTasks(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTasksByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksWithoutProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTasksWithoutProject(session.getUserId());
    }

    @Override
    @WebMethod
    public void assignToProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().assignToProject(session.getUserId(), id, projectId);
    }
}
