package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull String URL = "http://0.0.0.0:8080/ProjectEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<Project> findAllProjects();

    @Nullable
    @WebMethod
    List<Project> findAllProjectsByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findProjectByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception;

    @WebMethod
    void mergeProjectStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String status
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByStartDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByEndDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByStatus(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsBySearch(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "search") @Nullable String search
    ) throws Exception;
}
