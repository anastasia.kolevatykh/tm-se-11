package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

@WebService
public interface IDomainEndpoint {

    @NotNull String URL = "http://0.0.0.0:8080/DomainEndpoint?wsdl";

    @WebMethod
    void serialize(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void deserialize(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void saveJacksonJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void loadJacksonJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void saveJacksonXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void loadJacksonXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void marshalJaxbJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void unmarshalJaxbJson(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void marshalJaxbXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @WebMethod
    void unmarshalJaxbXml(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;
}
