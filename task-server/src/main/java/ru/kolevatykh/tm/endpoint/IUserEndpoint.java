package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @NotNull String URL = "http://0.0.0.0:8080/UserEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<User> findAllUsers(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "role") @Nullable String role,
            @WebParam(name = "isAuth") @Nullable Boolean isAuth
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserByLogin(
            @WebParam(name = "login") @Nullable final String login
    );
}
