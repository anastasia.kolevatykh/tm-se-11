package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.ServiceLocator;

public class AbstractEndpoint {

    @NotNull
    protected ServiceLocator serviceLocator;

    AbstractEndpoint() {
    }

    AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
