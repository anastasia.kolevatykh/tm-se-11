package ru.kolevatykh.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<User> findAllUsers(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public final User findUserById(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return null;
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findOneById(session.getUserId());
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final String role,
            @WebParam(name = "isAuth") @Nullable final Boolean isAuth
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().merge(login, password, RoleType.valueOf(role), isAuth);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().remove(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeAll();
    }

    @Nullable
    @Override
    public final User findUserByLogin(
            @WebParam(name = "login") @Nullable final String login
    ) {
        return serviceLocator.getUserService().findOneByLogin(login);
    }
}
