package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @NotNull String URL = "http://0.0.0.0:8080/TaskEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<Task> findAllTasks();

    @Nullable
    @WebMethod
    List<Task> findAllTasksByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception;

    @WebMethod
    void mergeTaskStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String status
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByStartDate(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByEndDate(
            @WebParam(name = "session") @Nullable final Session session

    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByStatus(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksBySearch(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "search") @Nullable String search
    ) throws Exception;

    @WebMethod
    void removeTasksWithProjectId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void removeProjectTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksWithoutProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void assignToProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;
}
