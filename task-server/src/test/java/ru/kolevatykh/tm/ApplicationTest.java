package ru.kolevatykh.tm;

import org.junit.Test;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.repository.ProjectRepository;
import ru.kolevatykh.tm.repository.TaskRepository;
import ru.kolevatykh.tm.repository.UserRepository;

import static org.junit.Assert.*;

public class ApplicationTest
{

    @Test
    public void shouldFindUserByLogin() {
        UserRepository userRepository = new UserRepository();

        assertNull(userRepository.findOneByLogin("demo"));

        userRepository.persist("demo", "pass", RoleType.USER,true);

        assertNotNull(userRepository.findOneByLogin("demo"));
    }

    @Test
    public void shouldAssignTaskToProject() {
        UserRepository userRepository = new UserRepository();
        userRepository.persist("demo", "pass", RoleType.ADMIN, true);

        User user = userRepository.findOneByLogin("demo");

        ProjectRepository projectRepository = new ProjectRepository();
        TaskRepository taskRepository = new TaskRepository();

        Project project = new Project("demo", "", null, null);
        project.setUserId(user.getId());
        Task task = new Task("demo", "", null, null);
        task.setUserId(user.getId());

        assertNull(taskRepository.findOneByName(user.getId(), "demo"));
        assertNull(projectRepository.findOneByName(user.getId(), "demo"));

        projectRepository.persist(user.getId(), "demo", "", null, null);
        taskRepository.persist(user.getId(), null, "demo", "", null, null);

        assertNotNull(taskRepository.findTasksWithoutProject(user.getId()));
    }
}
